/**
 * Email service are automated process that use Apex class
 * to process the contents, headers, and attachments of inbound
 * email.
 * This class is designed to handle vacancy email from different senders and automatically records 
 * content of each email into Vacancy Object
 * 
 * Author : Andy Yang
 */
global class CTCEmailToJobService implements Messaging.InboundEmailHandler {
	

	//Email Service entry method
    global Messaging.InboundEmailResult handleInboundEmail(
    	Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        
        System.debug('#CTCEmailToJobService#');
        System.debug('#email.subject#'+email.subject);
        System.debug('#email.htmlBody #'+email.htmlBody);
        System.debug('#email.plainTextBody #'+email.plainTextBody); 
        System.debug('#email.fromAddress  #'+email.fromAddress );
        
        //default return result
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        try {
        	
        	//parse email and do corresponding process, such making vacancy
            invokeEmailHandler(email);  
                
        } catch(Exception ee) {
            System.debug('---exception in CTCEmailToJobService----' + ee);
           
        }
        
        return result;
    }  
    
    /*
    * Parse the content of email and decide what action to do according to the content of email
    * 
    */
    private void invokeEmailHandler(Messaging.InboundEmail email) {
    	
        //create a corresponding IEmailHandler object, such as VacancyMaker object
        IEmailHandler handler = EmailHandlerFactory.build( email );
        
        if(handler != null ){
        	//execute handle processing
        	handler.handle();
        }
    }
}