/**
* This abstract class implements IVacancyParser and as a template class, it creates parse() method 
* to parse email to VacancyContent object
*
*/


public abstract class AbstractVacancyParser implements IVacancyParser{
	
	protected Messaging.InboundEmail email;
	
	
	public AbstractVacancyParser(){
		
	}
	
	
	
	public AbstractVacancyParser(Messaging.InboundEmail inEmail){
		this.email = inEmail;
	}
	
	public void setEmail( Messaging.InboundEmail inEmail ){
		this.email = inEmail;
	}
	
	/**
	* Parse and extract the details of enquiry email to VacancyContent object
	* return :
	* 	VacancyContent object
	*/
	public virtual VacancyContent parse(){
		
		if(email == null) return null;
		
		VacancyContent content = new VacancyContent( );
		content.jobCode = getJobCode();
		content.jobTitle = getJobTitle();
		content.description = getDescription();
		content.jobCategory = getJobCategory();
		content.jobType = getJobType();
		content.startDate = getStartDate();
		content.endDate = getEndDate();
		content.createDate = getCreateDate();
		content.postedBy = getPostedBy();
		content.shift = getShift();
		content.positions = getPositions();
		content.comment = getComment();
		content.client = getClient();
		content.clientLocation = getClientLocation();
		content.clientDivision = getClientDivision();
		content.clientDepartment = getClientDepartment();
		content.status = getStatus();
		content.reportTo = getReportTo();
		
		return content;
	}

	
	public abstract String getJobCode(); 
	public abstract String getJobTitle(); 
	public abstract String getDescription(); 
	public abstract String getJobCategory(); 
	public abstract String getJobType(); 
	public abstract Date getStartDate(); 
	public abstract Date getEndDate(); 
	public abstract Date getCreateDate(); 
	public abstract String getPostedBy(); 
	public abstract Integer getShift(); 
	public abstract Integer getPositions(); 
	public abstract String getComment(); 
	
	public abstract String getClient(); 
	public abstract String getClientLocation(); 
	public abstract String getClientDivision(); 
	public abstract String getClientDepartment(); 
	public abstract String getStatus(); 
	public abstract String getReportTo();
    
}