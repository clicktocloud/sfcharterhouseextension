@isTest
private class VacancyParserFactoryTest {
    
   static Map<String , Messaging.InboundEmail> emails  = new Map<String, Messaging.InboundEmail>();
   
   static{
       	Messaging.InboundEmail mail = new Messaging.InboundEmail();
       	//from Fieldglass
		mail.plainTextBody = 'A new Job Posting has been submitted';
	    emails.put('Fieldglass', mail);
	    //from domain
	    mail = new Messaging.InboundEmail();
	    mail.plainTextBody = 'The following Order has been re-opened';
	    emails.put('Comensura', mail);
	    
	    //unexpected email
	    mail = new Messaging.InboundEmail();
	    mail.plainTextBody = 'This is unexpected email\n';
	    emails.put('unknown', mail);
	    
	    /*
	    *by from address or subject
	    */
	    // Fieldglass email
	    mail = new Messaging.InboundEmail();
		mail.subject = 'New Job Posting submitted';
		mail.fromAddress = 'fieldglass@fieldglass.net' ;
		emails.put('Fieldglass-sub-from', mail);
		
		// Comensura email by subject
		mail = new Messaging.InboundEmail();
		mail.subject = 'position(s) required';
		mail.fromAddress = 'helpdesk@comensura.com.au';
		emails.put('Comensura-sub-from', mail);
	
		
	    
   }

   static testMethod void testGetVacancyParserName(){
		
		System.assertEquals('FieldglassVacancyParser' , VacancyParserFactory.getVacancyParserName( emails.get('Fieldglass') ));
		System.assertEquals('ComensuraVacancyParser', VacancyParserFactory.getVacancyParserName( emails.get('Comensura') ) );
		System.assertEquals('', VacancyParserFactory.getVacancyParserName( emails.get('unknown') ) );
	}
	
	static testMethod void testCreateVacancyParser( ){
	    System.assert(VacancyParserFactory.createVacancyParser('FieldglassVacancyParser') instanceof FieldglassVacancyParser);
	    System.assert(VacancyParserFactory.createVacancyParser('ComensuraVacancyParser') instanceof ComensuraVacancyParser);
	    System.assert(VacancyParserFactory.createVacancyParser('NotExistingParser') == null);
	}
	
	static testMethod void testGetVacancyParser(){
	    System.assert(VacancyParserFactory.getVacancyParser(emails.get('Fieldglass')) instanceof FieldglassVacancyParser);
	    System.assert(VacancyParserFactory.getVacancyParser(emails.get('Comensura')) instanceof ComensuraVacancyParser);
	    System.assert(VacancyParserFactory.getVacancyParser(emails.get('unknown')) == null);
	}
	

	static testMethod void testBuildVacancyParserByEmailSubjectAndFromAddress( ){
	    
	    System.assert(VacancyParserFactory.getVacancyParser(emails.get('Fieldglass-sub-from')) instanceof FieldglassVacancyParser);
	    System.assert(VacancyParserFactory.getVacancyParser(emails.get('Comensura-sub-from')) instanceof ComensuraVacancyParser);
	    
		
	}
}