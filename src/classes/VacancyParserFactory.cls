/**
* The factory provide some ways to create the corresponding instance of IVacancyParser for these emails 
* coming from :
* 1. Fieldglass
* 2. ...

* This class use apex reflection to create instance of parser by getting parser name from Parser Registion map 
* instead of directory new instance of parser. 
* as the result, we can dynamically configure parser for different type  of email.
*/

public with sharing class VacancyParserFactory {
	
	public static Map<String, String> parserKeyword = new Map<String, String>();
	static{
		//by keywords
		parserKeyword.put('A new Job Posting has been submitted' , 'FieldglassVacancyParser'); //for Fieldglass parser
		parserKeyword.put('The following Order has been re-opened' , 'ComensuraVacancyParser'); //for Comensura parser
	}
	
	public static Map<String, String> parserRegister = new Map<String, String>();
	
	//initialise the parser registion
	//TODO read data from custom settings to do initialization
	static{
		
		//by from address
		parserRegister.put('fieldglass@fieldglass.net' , 'FieldglassVacancyParser'); //for Fieldglass parser
		parserRegister.put('helpdesk@comensura.com.au' , 'ComensuraVacancyParser'); //for Comensura parser
		
		//by subject
		parserRegister.put('New Job Posting submitted' , 'FieldglassVacancyParser'); //for Fieldglass vacancy
		parserRegister.put('position(s) required' , 'ComensuraVacancyParser'); //for Comensura parser
	
		
	}
	
	/*
	* Get an object of vacancy parser according to the email
	* Param:
	* 	email - Messaging.InboundEmail
	* return:
	*	a parser object - IVacancyParser
	*/
	public static IVacancyParser getVacancyParser( Messaging.InboundEmail email ){
		IVacancyParser parser = null;
		
		String parserName = getVacancyParserName( email );
		
		//new an instance of parser depending on parser name
		parser = createVacancyParser(parserName);
		if(parser != null){
			parser.setEmail( email );
		}
		
		return parser;
		
	}
	
	/*
	* Get the class name of vacancy parser according to the email
	* Param:
	* 	email - Messaging.InboundEmail
	* return:
	*	the class name of parser - String
	*/
	public static String getVacancyParserName( Messaging.InboundEmail email ){
	    
	    String parserName = '';
	    
	    if(email == null ) return parserName;
		
		String fromAddress = email.fromAddress;
		String subject = email.subject;
		String plainTextBody = email.plainTextBody;
		
		//get parser name by subject
		for( String sub :  parserKeyword.keySet() ){
			
			if( plainTextBody != null && plainTextBody.contains( sub) ){
				parserName = parserKeyword.get(sub);
				break;
			}	
		}
		
		
		//get parser name by from address
		if(parserName == null || parserName == ''){
		    if(fromAddress != null)
			    parserName = parserRegister.get( fromAddress);
		}
		
		//get parser name by subject
		if(parserName == null || parserName == ''){

			for( String sub :  parserRegister.keySet() ){
				
				if( subject != null && subject.contains( sub) ){
					parserName = parserRegister.get(sub);
					break;
				}	
			}
		}
		
		return parserName;
		
	}
	
	/*
	* Create an instance of parser according to the class name of parser
	* Param:
	* 	parsesr name - String
	* return:
	*	a parser object - IVacancyParser
	*/
	public static IVacancyParser createVacancyParser(String parserName){
		
		IVacancyParser parser  = null;
		
		if(parserName != null && parserName != ''){
			try{
				Type parserType = Type.forName('', parserName);
				if(parserType != null){
				    parser =(IVacancyParser) parserType.newInstance();
				}
				 
			}catch(TypeException e){
				System.debug('Failed to create an instance of parser ' + parserName);
				System.debug(e);
				//TODO more exception handling
				
			}
		}else{
			System.debug('Parser class name can not be empty !');
		}
		
		return parser;
	}
	
}