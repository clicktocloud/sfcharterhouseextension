/**
* VacancyMaker is a implementation of IEmailHandler interface, which can parse vacancy details 
* from one email, record the information of vacancy to Vacancy object.
* Each VacancyMaker MUST have a reference of IVacancyParser to execute parsing email content.
*/

public with sharing class VacancyMaker implements IEmailHandler{
	
	
	
	private static final String CLIENT_ACCOUNT_TYPE = 'Client';
	private static final String CLIENT_CONTACT_TYPE = 'Contact';
	
	private static final String VACANCY_API_NAME = 'PeopleCloud1__Placement__c';
	private static final String CONTRACT_VACANCY_RECORDTYPE = 'Contract Vacancy';
	private static final String PERMANENT_VACANCY_RECORDTYPE = 'Permanent Vacancy';
	private static final String TEMPORARY_VACANCY_RECORDTYPE = 'Temporary Vacancy';
	
	private static final String JOB_ON_STAGE = 'Job on';
	
	private static final String ADMIN_PROFILE_NAME = 'Custom - System Administrator';
	private static final String ADMIN_NAME = 'Charterhouse Partnership Administrator';
	
	private static final String TASK_STATUS='Completed';
	private static final String TASK_PRIORITY = 'Normal';
	
	private Id clientRecordTypeId= null;
	private Id clientContactRecordTypeId= null;
	private Id ownerQueueId = null;
	
	//parser for enquiry email
	private IVacancyParser parser;
	
	private Messaging.InboundEmail email;
	
	public VacancyMaker() { 
		clientRecordTypeId = getRecordType(CLIENT_ACCOUNT_TYPE, 'Account');
		clientContactRecordTypeId = getRecordType(CLIENT_CONTACT_TYPE, 'Contact');
		ownerQueueId = getOwnerQueueIdByDeveloperName('Emailed_Jobs_CHP');
	}
	
	
	public VacancyMaker(IVacancyParser parser) {
		this();
		this.parser = parser;
	}
	
	public void setEmail(Messaging.InboundEmail srcEmail){
		this.email = srcEmail;	
	}
	
	public Messaging.InboundEmail getEmail(){
		return this.email;
	}
	
	//execute handle processing
	public void handle(){
		if(this.parser != null){
			
			//invoke make method of this object
			this.make();
		}
	}
	
	public void make() {
		
		//parse vacancy email to VacancyContent object for next processing
		VacancyContent content = parser.parse();
		
		System.debug('-----------------------------------------');
		System.debug(content.toString());
		System.debug('-----------------------------------------');
		//make enquiry and record it to Enquiry object;
		PeopleCloud1__Placement__c vacancy = make(content);
		
		if(vacancy != null){
			//create activity for this email vacacny	
			recordTask( vacancy,  getEmail() );
				
		}
		
	}
	
	
	/**
	* Record a task/activity according to 
	*/
	public Task recordTask(PeopleCloud1__Placement__c vacancy,  Messaging.InboundEmail email ){
		/*
		Assigned To: The System Admin
		Related To: The Vacancy created
		Name: The Client Contact
		Subject: "Vacancy Email:" + email subject
		Due Date: Today()
		Comments: content of the email
		Status: Complete
		Priority: Normal
		
		*/
		
		Task task = new Task();
		task.OwnerId = getAdministrator();
		task.WhatId = vacancy.id;
		task.WhoId = vacancy.PeopleCloud1__Vacancy_Contact__c;
		task.Subject = 'Vacancy Email:' + email.subject;
		task.ActivityDate = Date.today(); //System.now();
		task.Description = email.plainTextBody;
		task.status = TASK_STATUS;
		task.Priority = TASK_PRIORITY;
	    try{
	    	 insert task;
	    	 System.debug('Success to record task ['+task.subject+'] to ['+task.OwnerId+']');
	    }catch(Exception e){
	    	System.debug(e);
	    	task = null;
	    }
	   
	   return task;
	}
	
	/**
	* Make vacancy according to the  VacancyContent object
	* params:
	* 	content - VacancyContent
	* return:
	*	 object - PeopleCloud1__Placement__c
	*/
	public PeopleCloud1__Placement__c make(VacancyContent content){
		PeopleCloud1__Placement__c vacancy = null;
		
		Id vid = getVacancyId(content.jobCode);
		if(vid == null){
			//build an Vacancy with basic information
			vacancy = buildVacancy( content );

			//insert vacancy to database
			vacancy = insertVacancy(vacancy);
			
		}
				
		return vacancy;	
	}
	
	/**
	* Build a vacancy object with properties of VacancyContent object
	*
	*/
	public PeopleCloud1__Placement__c buildVacancy( VacancyContent content ){
		
		PeopleCloud1__Placement__c vacancy = new  PeopleCloud1__Placement__c();
      
        //set vacancy properties 
        vacancy.ownerId = ownerQueueId;
        
        //code
        vacancy.Purchase_Order_Number_CHP__c  = content.jobCode;
        
        //name
        vacancy.name = content.jobTitle;
       
        //description
        vacancy.PeopleCloud1__Placement_Description__c = content.description +' ' + content.comment;
        //category
        //vacancy.PeopleCloud1__Category__c = content.jobCategory;
        
        //record type
        vacancy.RecordTypeID = getVacancyRecordType(content);
        
        //positions
        vacancy.Number_of_Positions_CHP__c = content.positions;
        
        //start date
        vacancy.PeopleCloud1__Start_Date__c = content.startDate;
        //end date
        vacancy.PeopleCloud1__End_Date__c = content.endDate;
        //create date
        //vacancy.PeopleCloud1__Placement_Date__c = content.createDate;
        
        //client
        if(content.client != null && !content.client.equals('') ){
        	 Id clientId = getClientByName(content.client);
        	 if(clientId != null){
        	 	vacancy.PeopleCloud1__Company__c = clientId;
        	 	
        	 	 //contact
        		vacancy.PeopleCloud1__Vacancy_Contact__c = getClientContact(content.postedBy, clientId);
        		
        		//Line Manager (reports to)
        		vacancy.Line_Manager_CHP__c = getClientContact(content.reportTo, clientId);
        	 }
        }
        
        
        //status
        vacancy.PeopleCloud1__Stage__c = getStage(content.status);
        
        return vacancy;
	}
	

	/**
	* Get vacancy stage by specified status 
	*/
	public String getStage(String status){
		
		if(status != null && status.equals(VacancyContent.OPEN_STATUS)){
			return JOB_ON_STAGE;
		}
		
		return '';
	}
	
	/**
	* Get the Id of owner queue by name
	*
	*/
	public Id getOwnerQueueIdByDeveloperName(String queueName){
		
		
	    
	    Id retId = null;
		
		try {
			retId = [select id,name from Group where DeveloperName = :queueName and Type = 'Queue' limit 1].id;
			
		} catch(QueryException e) { 
			System.debug(e);
		}
		
		return retId;
		
	}
	
	/**
	* get the Id of vacancy by jobCode
	* params:
	*	property - String
	* return:
	* 	Id of vacancy
	*/
	public Id getVacancyId(String jobCode){
	    
	    Id retId = null;
		
		try {
			retId = [select id,name from PeopleCloud1__Placement__c where Purchase_Order_Number_CHP__c = :jobCode limit 1].id;
			
		} catch(QueryException e) { 
			System.debug(e);
		}
		
		return retId;
		
	}

	/**
	* insert a new Vacancy
	*/
	public PeopleCloud1__Placement__c insertVacancy( PeopleCloud1__Placement__c vacancy ){

		if(vacancy.name != null && ! vacancy.name.equals('') ){
			  
			try{
				insert vacancy;
			}catch(Exception e){
				System.debug(e);
				return null;
			}   
		}		
		return vacancy;
	}
	
	/*
	 *  Get Client Id by name
	 */
	public Id getClientByName(String name) {
		
		Id clientId  = null;
		try {
			
			clientId = [select id, name from Account where name = :name and RecordTypeID = :clientRecordTypeId  limit 1].id;
		} catch(Exception e) { 
			System.debug(e);
		}
		
		return clientId;
	} 
	
	
	
	/*
	 *  Get User Id by name
	 */
	public Id getClientContact(String name, Id clientId) {
		
		if(name == null || name.trim().equals('')){
			return null;
		}
		
		Id contactId  = null;
		try {
			
			contactId = [select id, name from Contact where name = :name and RecordTypeID = :clientContactRecordTypeId and AccountId=:clientId limit 1].id;
		} catch(Exception e) { 
			System.debug(e);
		}
		
		return contactId;
	} 
	
	public Id getAdministrator(){
		Id aid = null;
		try{
			Id sys_adm_profile = [SELECT Id FROM Profile WHERE Name = :ADMIN_PROFILE_NAME limit 1].Id;
			aid = [SELECT id,name FROM User WHERE ProfileId = :sys_adm_profile and name = :ADMIN_NAME limit 1].id;
		}catch(Exception e){
			System.debug(e);
		}
		
		return aid;
		
	}
	
	/**
	* Get vacancy record type by VacancyContent object
	*/
	public Id getVacancyRecordType(VacancyContent content){
		Id vid = null;
		String vacancyRecordType = content.jobType;
		
		//if jobType was not extracted from email
		if(null == vacancyRecordType || vacancyRecordType.trim().equals('')){
			
			vacancyRecordType = PERMANENT_VACANCY_RECORDTYPE;
	
		}
		
		vid = getRecordType(vacancyRecordType, VACANCY_API_NAME);
		
		//if record type can not be got by specified name, then set the default value of record type
		if(vid == null){
			
			
			vacancyRecordType = PERMANENT_VACANCY_RECORDTYPE;
			
			
			vid = getRecordType(vacancyRecordType,VACANCY_API_NAME);
		}
		
		return vid;
	}
	
	
	
	public Id getRecordType(String targetName, String targetSobjectType){
		Id rtId = null;
		try{
			rtId =[select Id from RecordType where (Name = :targetName) and (SobjectType = :targetSobjectType)].id;
		}catch(Exception e){
			System.debug(e);
		}
		return rtId;
	}
}