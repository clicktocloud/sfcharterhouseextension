/**
* This class extends the AbstractVacancyParser and parser the email from Fieldglass
* Please refer to AbstractVacancyParser
*/

/*
Email Text Content:

Job Posting ID

NSWEJP00001376

Job Posting Title

Personal Assistant

Description

The role provide high level confidential executive, administrative and
project support to the Executive Director.
Intermediate to advanced level of Word and Excel.
Critical to this role is exceptional interpersonal, organisational and time
management skills.

Job Posting Start Date

24/05/2016

Job Posting End Date

18/07/2016

Business Unit

Asset Management

Site

State Office-Sydney
Posting Information

Job Posting Owner

Michael Brown



*/
public with sharing class FieldglassVacancyParser extends AbstractVacancyParser{
	
	private static String MAIL_TYPE = 'FIELDGLASS';
	
	
	
	protected String plainTextBody = '';
	
	public FieldglassVacancyParser( ){
		super( );
	}
  
	public FieldglassVacancyParser(Messaging.InboundEmail inEmail){
		super(inEmail);
	}
	
	public override VacancyContent parse( ){
		if(email == null) return null;
		
		plainTextBody = email.plainTextBody;
		if(plainTextBody == null)
			return null;
		
		plainTextBody = plainTextBody.replace('*','');
		
		VacancyContent content = super.parse( );
		
		content.mailType = MAIL_TYPE;
		
		return content;
	}
	
	public override String getJobCode(){
		return EmailServiceHelper.getValueInblockByTitle('Job Posting ID\n', '' ,'Job Posting Title\n', plainTextBody);
	}
	public override String getJobTitle(){
		return EmailServiceHelper.getValueInblockByTitle('Job Posting Title\n', '' ,'Description\n', plainTextBody);
	}
	public override String getDescription(){
		return EmailServiceHelper.getValueInblockByTitle('Description\n', '' ,'Job Posting Start Date\n', plainTextBody);
	}
	public override String getJobCategory(){
		return '';
	}
	public override String getJobType(){
		return '';
	}
	public override Date getStartDate(){
		Date retDate = null;
		String s= EmailServiceHelper.getValueInblockByTitle('Job Posting Start Date\n', '' ,'Job Posting End Date\n', plainTextBody);
		if(s.equals('')){
			
		}else{
			try{
				retDate = Date.parse(s);
			}catch(Exception e){
				System.debug(e);
			}
		}
		return retDate;
	}
	public override Date getEndDate(){
		Date retDate = null;
		String s = EmailServiceHelper.getValueInblockByTitle('Job Posting End Date\n', '' ,'Business Unit\n', plainTextBody);
		if(s.equals('')){
			
		}else{
			try{
				retDate = Date.parse(s);
			}catch(Exception e){
				System.debug(e);
			}
		}
		return retDate;
	
	}
	public override Date getCreateDate(){
		return null;
	}
	public override String getPostedBy(){
		return EmailServiceHelper.getValueInblockByTitle('Job Posting Owner\n', '' ,'This notification was sent by', plainTextBody);
	}
	public override Integer getShift(){
		return 0;
	}
	public override Integer getPositions(){
		return 0;
	}
	public override String getComment(){
		return '';
	}
	
	public override String getClient(){
		return 'Fieldglass';
	}
	public override String getClientLocation(){
		return EmailServiceHelper.getValueInblockByTitle('Site\n', '' ,'Job Posting Owner\n', plainTextBody);
	}
	public override String getClientDivision(){
		return '';
	}
	public  override String getClientDepartment(){
		return EmailServiceHelper.getValueInblockByTitle('Business Unit\n', '' ,'Site\n', plainTextBody);
	}
	
	public  override String getStatus(){
		return VacancyContent.OPEN_STATUS;
	}
	
	public override String getReportTo(){
		return '';
	}
}