public class VacancyContent {
		public  static String OPEN_STATUS = 'Open';
	
		public VacancyContent(){}
		public String mailType;
		
		public String jobCode;
		public String jobTitle;
		public String description;
		public String jobCategory;
		public String jobType;
		
		
		public Date startDate;
		public Date endDate;
		public Date createDate;
		public String postedBy;
		
		public Integer shift;
		public Integer positions;
		
		public String comment;
		
		public String client;
		public String clientLocation;
		public String clientDivision;
		public String clientDepartment;
		
		public String status;
		
		public String reportTo;
		
		public override String toString(){
			return 'Vacancy Content:\n'
				+ '\tmailType : ' + mailType + '\n'
				+ '\tjobCode : ' + jobCode + '\n'
				+ '\tjobTitle : ' + jobTitle + '\n'
				+ '\tdescription : ' + description + '\n'
				+ '\tjobCategory : ' + jobCategory + '\n'
				+ '\tjobType : ' + jobType + '\n'
				+ '\tstartDate : ' + startDate + '\n'
				+ '\tendDate : ' + endDate + '\n'
				+ '\tcreateDate : ' + createDate + '\n'
				+ '\tpostedBy : ' + postedBy + '\n'
				+ '\tshift : ' + shift + '\n'
				+ '\tpositions : ' + positions + '\n'
				+ '\tcomment : ' + comment + '\n'
				+ '\tclient : ' + client + '\n'
				+ '\tclientLocation : ' + clientLocation + '\n'
				+ '\tclientDivision : ' + clientDivision + '\n'
				+ '\tclientDepartment : ' + clientDepartment + '\n'
				+ '\tstatus : ' + status + '\n'
				+ '\trepotTo : ' + reportTo + '\n'
				;
				
		}
			
    
}