public with sharing class EmailServiceHelper {
	
	
	public static String getValueInlineByTitle(String title, String delimiter, String src){
    	
    	if(title == null || src ==  null) return '';
    	
    	String ptn = '(?im)(?<=\\b' + title+ '\\s{0,5}' + delimiter + ').+$';
    	
    	String ret = substringByRegex( src, ptn);
    	if(ret != null) ret = ret.trim();
    	
    	return ret;
    }
    
    public static String getValueInblockByTitle(String title, String delimiter, String endKeyword, String src){
    	
    	if(title == null || src ==  null) return '';
    	
    	String ptn = '(?is)(?<=\\b' + title+ '\\s{0,5}' + delimiter + ').+';
    	
    	if(endKeyword != null && endKeyword !=''){
    		ptn = ptn + '(?=' + endKeyword + ')';
    	}
    	
    	String ret = substringByRegex( src, ptn);
    	if(ret != null) ret = ret.trim();
    	
    	return ret;
    }
	
	public static String substringByRegex( String src, String regexp){
		
		String ret = '';
		
		try{
			Pattern ptn = Pattern.compile(regexp);
		
			Matcher matcher = ptn.matcher(src);
		
			if( matcher.find() ){
				ret = matcher.group( );
			}
		}catch(Exception e){
			System.debug(e);
		}
		
		return ret ;
		
	}
	
	
	/*
    public static void sendExceptionDetails(String errorMessage, String subject){
    
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
    //email address
    //map<String,Email_Notification__c> emailNoteCustSetting = Email_Notification__c.getAll();
    //list<String> emailList = new list<String>();
    //emailList.addAll(emailNoteCustSetting.keySet());
    
   // mail.setReplyTo('');
    
    String[] toAddress = new String[] {'yanglp7479@gmail.com'};      
    mail.setToAddresses(toAddress);
    
    //subject
    mail.setSubject(subject);
    mail.setUseSignature(false);
    
    //body
   //mail.setHtmlBody(errorMessage);
   mail.setPlainTextBody(errorMessage);
    
    //sending the mail
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
  }*/
}