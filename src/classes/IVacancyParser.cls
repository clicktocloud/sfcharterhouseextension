/**
* This interface declares a range methods which can be implemented to get details from in-come email
* 	String getListingId();
*	String getEmailAddress();
*	String getPropertyAddress();
*	String getFirstName();
*	String getLastName();
*	String getPhone();
*	String getAboutMe();
*	String getQuestion();
*	String getComment();
*
*/

public interface IVacancyParser {
	
	

	VacancyContent parse();
	
	void setEmail(Messaging.InboundEmail email);
	
   	String getJobCode();
	String getJobTitle();
	String getDescription();
	String getJobCategory();
	String getJobType();
	
	Date getStartDate();
	Date getEndDate();
	Date getCreateDate();
	String getPostedBy();
	Integer getShift();
	Integer getPositions();
	String getComment();
	String getClient();
	String getClientLocation();
	String getClientDivision();
	String getClientDepartment();
	String getStatus();
	String getReportTo();
}