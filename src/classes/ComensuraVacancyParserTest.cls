@isTest
private class ComensuraVacancyParserTest {

   	private static Messaging.InboundEmail email;
    private static ComensuraVacancyParser parser; 
    private static VacancyContent content;
   
    private static void createFixture(){
        email = new Messaging.InboundEmail();
        email.subject = 'Order #907918 Re-opened (Administration Support Officer - Planning & Design for 1 position(s) required, starting 16 May 2016 (8:30 AM)';
        email.fromAddress = 'somebody@somedomain.com';
        email.plainTextBody = 'The following Order has been re-opened.\n'
			+'Order Num\n'
			+'907918\n'
			+'Client\n'
			+'Boral Ltd\n'
			+'Building\n'
			+'Dandenong Civic Centre, 225 Lonsdale Street, Dandenong, 3175\n'
			+'Client Division\n'
			+'City Planning, Design and Amenity\n'
			+'Client Department\n'
			+'Planning and Design\n'
			+'Job Category\n'
			+'Secretarial and Administration\n'
			+'Job Title\n'
			+'Administration Support Officer - Planning & Design\n'
			+'Job Type\n'
			+'Temporary\n'
			+'Shift\n'
			+'0\n'
			+'# Positions\n'
			+'1\n'
			+'Start Date\n'
			+'16/05/2016\n'
			+'Start Time\n'
			+'08:30\n'
			+'End Date\n'
			+'01/07/2016\n'
			+'End Time\n'
			+'17:00\n'
			+'Order Owner\n'
			+'Ali, Riyaz\n'
			+'Reports To\n'
			+'Stewart, Will\n'
			+'Date Created\n'
			+'12/05/2016\n'
			+'Status\n'
			+'Open\n'
			+'Comments\n'
			+'Previous planning support experience would be desirable\n'
			+'To log into Comensura.net v4.0 automatically, click the link below.';
    }
    
    static{
        createFixture( );
        
        parser = new ComensuraVacancyParser(email);
        
        content = parser.parse();
    }
    
    
    
    static testMethod void testGetJobCode(){
        
        System.assertEquals('907918',  parser.getJobCode());
        
    }
    static testMethod void testGetJobTitle(){
        
        System.assertEquals('Administration Support Officer - Planning & Design', parser.getJobTitle());
        
    }
    
    static testMethod void testGetJobType(){
        
        System.assertEquals('Temporary Vacancy', parser.getJobType());
        
    }
    
    static testMethod void testGetStartDate(){
        
        System.assertEquals( Date.parse('16/05/2016'), parser.getStartDate());
        
    }
    
    static testMethod void testGetEndDate(){
        
        System.assertEquals(Date.parse('01/07/2016'), parser.getEndDate());
        
    }
    
    static testMethod void testGetPostedBy(){
        
        System.assertEquals('Riyaz Ali', parser.getPostedBy());
        
    }
    
     static testMethod void testGetReportTo(){
        
        System.assertEquals('Will Stewart', parser.getReportTo());
        
    }
    
     static testMethod void testGetPositions(){
        
        System.assertEquals(1, parser.getPositions());
        
    }
    
    static testMethod void testGetComment(){
        
        System.assertEquals('Previous planning support experience would be desirable', parser.getComment());
        
    }
    
     static testMethod void testGetClient(){
        
        System.assertEquals('Boral Ltd', parser.getClient());
        
    }
    
    
     static testMethod void testGetStatus(){
        
        System.assertEquals('Open', parser.getStatus());
        
    }
    
    
    
    static testMethod void testParse(){
        System.assert(content != null);
        System.assertEquals('907918', content.jobCode);
        System.assertEquals('Administration Support Officer - Planning & Design', content.jobTitle);
        System.assertEquals(1, content.positions);
        System.assertEquals('Open', content.status);
        System.assertEquals('Boral Ltd', content.client);
        System.assertEquals('Riyaz Ali', content.postedBy);
        System.assertEquals('Will Stewart', content.reportTo);
        
    }
    
    static testMethod void testFormatName(){
    	System.assertEquals('a b c',parser.formatName('c,b,a'));
    }
    
}