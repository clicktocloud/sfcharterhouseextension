/**
* This class extends the AbstractVacancyParser and parser the email from Fieldglass
* Please refer to AbstractVacancyParser
*/
/*
Email Text Content:

The following Order has been re-opened.

Order Num

907918

Client

City of Greater Dandenong

Building

Dandenong Civic Centre, 225 Lonsdale Street, Dandenong, 3175

Client Division

City Planning, Design and Amenity

Client Department

Planning and Design

Job Category

Secretarial and Administration

Job Title

Administration Support Officer - Planning & Design

Job Type

Temporary

Shift

0

# Positions

1

Start Date

16/05/2016

Start Time

08:30

End Date

01/07/2016

End Time

17:00

Order Owner

Stewart, Will

Reports To

Stewart, Will

Date Created

12/05/2016

Status

Open

Comments

Previous planning support experience would be desirable


*/
public with sharing class ComensuraVacancyParser extends AbstractVacancyParser{
	
	public static String LABEL_SUFFIX = '';
	public static String JOBCODE_LABEL = 'Order Num\n';
	public static String JOBTITLE_LABEL = 'Job Title\n';
	public static String JOBCATEGORY_LABEL = 'Job Category\n';
	public static String JOBTYPE_LABEL = 'Job Type\n';
	public static String STARTDATE_LABEL = 'Start Date\n';
	public static String STARTTIME_LABEL = 'Start Time\n';
	public static String ENDDATE_LABEL = 'End Date\n';
	public static String ENDTIME_LABEL = 'End Time\n';
	public static String CREATEDATE_LABEL = 'Date Created\n';
	public static String POSTEDBY_LABEL = 'Order Owner\n';
	public static String SHIFT_LABEL = 'Shift\n';
	public static String POSITION_LABEL = 'Positions\n';
	public static String COMMENT_LABEL = 'Comments\n';
	public static String CLIENT_LABEL = 'Client\n';
	public static String CLIENTLOCATION_LABEL = 'Building\n';
	public static String CLIENTDIVISION_LABEL = 'Client Division\n';
	public static String CLIENTDEPARTMENT_LABEL = 'Client Department\n';
	public static String STATUS_LABEL = 'Status\n';
	public static String REPORTTO_LABEL = 'Reports To\n';
	
	private static String MAIL_TYPE = 'COMENSURA';
	private static String JOBTYPE_SUFFIX = 'Vacancy';
	
	
	protected String plainTextBody = '';
	
	public ComensuraVacancyParser( ){
		super( );
	}
  
	public ComensuraVacancyParser(Messaging.InboundEmail inEmail){
		super(inEmail);
	}
	
	public override VacancyContent parse( ){
		if(email == null) return null;
		
		plainTextBody = email.plainTextBody;
		if(plainTextBody == null)
			return null;
		
		plainTextBody = plainTextBody.replace('*','');
		
		VacancyContent content = super.parse( );
		
		content.mailType = MAIL_TYPE;
		
		return content;
	}
	
	
	
	public override String getJobCode(){
		return EmailServiceHelper.getValueInblockByTitle(JOBCODE_LABEL, LABEL_SUFFIX ,CLIENT_LABEL, plainTextBody);
	}
	public override String getJobTitle(){
		return EmailServiceHelper.getValueInblockByTitle(JOBTITLE_LABEL, LABEL_SUFFIX ,JOBTYPE_LABEL, plainTextBody);
	}
	public override String getDescription(){
		return '';
	}
	public override String getJobCategory(){
		return EmailServiceHelper.getValueInblockByTitle(JOBCATEGORY_LABEL, LABEL_SUFFIX ,JOBTITLE_LABEL, plainTextBody);
	}
	public override String getJobType(){
		String jobType = EmailServiceHelper.getValueInblockByTitle(JOBTYPE_LABEL, LABEL_SUFFIX ,SHIFT_LABEL, plainTextBody);
		if(jobType != null && !jobType.trim().equals('')){
			
			if(jobType.trim().toUpperCase().startsWith('TEMP')){
				jobType = 'Temporary';	
			}
			
			jobType += ' ' + JOBTYPE_SUFFIX;
		}
		return jobType;
	}
	
	public override Date getStartDate(){
		Date retDate = null;
		String s = EmailServiceHelper.getValueInblockByTitle(STARTDATE_LABEL, LABEL_SUFFIX ,STARTTIME_LABEL, plainTextBody);
		if(s.equals('')){
			
		}else{
			try{
				retDate = Date.parse(s);
			}catch(Exception e){
				System.debug(e);
			}
		}
		return retDate;
	}
	public override Date getEndDate(){
		Date retDate = null;
		String s = EmailServiceHelper.getValueInblockByTitle(ENDDATE_LABEL, LABEL_SUFFIX ,ENDTIME_LABEL, plainTextBody);
		if(s.equals('')){
			
		}else{
			try{
				retDate = Date.parse(s);
			}catch(Exception e){
				System.debug(e);
			}
		}
		return retDate;
	}
	public override Date getCreateDate(){
		Date retDate = null;
		String s = EmailServiceHelper.getValueInblockByTitle(CREATEDATE_LABEL, LABEL_SUFFIX ,'Status\n', plainTextBody);
		if(s.equals('')){
			
		}else{
			try{
				retDate = Date.parse(s);
			}catch(Exception e){
				System.debug(e);
			}
		}
		return retDate;
	}
	public override String getPostedBy(){
		String poster = EmailServiceHelper.getValueInblockByTitle(POSTEDBY_LABEL, LABEL_SUFFIX ,REPORTTO_LABEL, plainTextBody);
		
	 	return formatName(poster);
	}
	public override Integer getShift(){
		String s = EmailServiceHelper.getValueInblockByTitle(SHIFT_LABEL, LABEL_SUFFIX ,POSITION_LABEL, plainTextBody);
		s = s.replaceAll('\\D+','');
		if(s.equals('')){
			return 0;
		}else{
			return Integer.valueOf(s);
		}
	}
	
	public override Integer getPositions(){
		String s = EmailServiceHelper.getValueInblockByTitle(POSITION_LABEL, LABEL_SUFFIX ,STARTDATE_LABEL, plainTextBody);
		
		s = s.replaceAll('\\D+','');
		if(s.equals('')){
			return 0;
		}else{
			return Integer.valueOf(s);
		}
	}
	public override String getComment(){
		return EmailServiceHelper.getValueInblockByTitle(COMMENT_LABEL, LABEL_SUFFIX ,'To log into Comensura', plainTextBody);
	}
	
	public override String getClient(){
		return EmailServiceHelper.getValueInblockByTitle(CLIENT_LABEL, LABEL_SUFFIX ,CLIENTLOCATION_LABEL, plainTextBody);
	}
	public override String getClientLocation(){
		return EmailServiceHelper.getValueInblockByTitle(CLIENTLOCATION_LABEL, LABEL_SUFFIX ,CLIENTDIVISION_LABEL, plainTextBody);
	}
	public override String getClientDivision(){
		return EmailServiceHelper.getValueInblockByTitle(CLIENTDIVISION_LABEL, LABEL_SUFFIX ,CLIENTDEPARTMENT_LABEL, plainTextBody);
	}
	public  override String getClientDepartment(){
		return EmailServiceHelper.getValueInblockByTitle(CLIENTDEPARTMENT_LABEL, LABEL_SUFFIX ,JOBCATEGORY_LABEL, plainTextBody);
	}
    
	public  override String getStatus(){
		String status = EmailServiceHelper.getValueInblockByTitle(STATUS_LABEL, LABEL_SUFFIX ,COMMENT_LABEL, plainTextBody);
		if(status == null || status.trim().equals('')){
			status = VacancyContent.OPEN_STATUS;
		}
		
		return status;
	}
	
	public override String getReportTo(){
		String poster = EmailServiceHelper.getValueInblockByTitle(REPORTTO_LABEL, LABEL_SUFFIX ,CREATEDATE_LABEL, plainTextBody);
		
	 	return formatName(poster);
	}
	
	public String formatName(String originalName){
		String retName ='';
		if(originalName != null && originalName.contains(',')){
	 		String[] names = originalName.split(',');
	
	 		for(Integer i = names.size() -1 ; i >= 0 ; i --){
	 			if(retName.equals('')){
	 				retName = names[i].trim();
	 			}else{
	 				retName += ' ' + names[i].trim();
	 			}
	 		}
	 	}else{
	 		retName = originalName;
	 	}
	 	
	 	return retName;
	}
}