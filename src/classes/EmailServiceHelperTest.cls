@isTest
private class EmailServiceHelperTest {

      static testMethod void testSubstringByRegexp() {
      	
      	System.assertEquals('Name',EmailServiceHelper.substringByRegex(' Email: andy@clicktocloud.com \nName : Andy Yang', 'Name'));
       
        
    }
    
    static testMethod void testGetValueInlineByTitle(){
    	 String src = ' Email: andy@clicktocloud.com \nName : Andy Yang';
		System.assertEquals( EmailServiceHelper.getValueInlineByTitle( 'email', ':', src),
								'andy@clicktocloud.com');
    }
    
    static testMethod void testGetValueInblockByTitle(){
    	String src = ' Email:\nandy@clicktocloud.com \nName :\nAndy Yang';
		System.assertEquals( EmailServiceHelper.getValueInblockByTitle( 'email', ':','\nName', src),
								'andy@clicktocloud.com');
    }
}