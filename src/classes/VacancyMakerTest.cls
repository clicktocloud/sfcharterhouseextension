/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VacancyMakerTest {
	static VacancyMaker defaultMaker = null;
	
	static{
		
		//for vacancy
		PeopleCloud1__Placement__c vacancy = new  PeopleCloud1__Placement__c();
        vacancy.Purchase_Order_Number_CHP__c  = '12345';
        vacancy.name = 'test job';
        insert vacancy;
        
        //for queue
        //Group g = new Group(name='Emailed Jobs', DeveloperName='Emailed_Jobs_CHP',type='Queue');
        //insert g;
       
       
       
       defaultMaker = new VacancyMaker();
       
        
	}
	
	static testMethod void testBuildVacancy(){
		VacancyContent vc = new VacancyContent();
		vc.jobCode = '0000';
		vc.jobTitle = 'test job 0000';
		vc.jobType='Temporary Vacancy';
		vc.status = 'Open';
		PeopleCloud1__Placement__c vacancy = defaultMaker.buildVacancy(vc);
		System.assert(vacancy != null);
		System.assertEquals(vacancy.RecordTypeId, defaultMaker.getRecordType('Temporary Vacancy','PeopleCloud1__Placement__c'));
		System.assertEquals(vacancy.PeopleCloud1__Stage__c, 'Job on');
	}

	static testMethod void testGetVacancyId(){
		System.assert(defaultMaker.getVacancyId('12345') != null);
		System.assert(defaultMaker.getVacancyId('23123') == null);
	}
    
    static testMethod void testGetOwnerQueueIdByName(){
    	System.assert(defaultMaker.getOwnerQueueIdByDeveloperName('Emailed_Jobs_CHP') != null);
    }
    
    static testMethod void testGetStage(){
    	System.assertEquals('Job on',defaultMaker.getStage(VacancyContent.OPEN_STATUS));
    	System.assertEquals('',defaultMaker.getStage('other'));
    }
    
    static testMethod void testInsertVacancy(){
    	PeopleCloud1__Placement__c vacancy = new  PeopleCloud1__Placement__c();
        vacancy.Purchase_Order_Number_CHP__c  = '12345';
        vacancy.name = 'test job';
        System.assert(defaultMaker.insertVacancy(vacancy) != null);
    }
    
    
    static testMethod void testGetRecordType(){
    	System.assert(defaultMaker.getRecordType('Client', 'Account')!=null);
    	System.assert(defaultMaker.getRecordType('Temporary Vacancy','PeopleCloud1__Placement__c')!=null);
    }
   
   static testMethod void testGetVacancyRecordType(){
   		VacancyContent vc = new VacancyContent();
   		vc.jobType='Temporary Vacancy';
   		System.assertEquals(defaultMaker.getRecordType('Temporary Vacancy','PeopleCloud1__Placement__c'),defaultMaker.getVacancyRecordType(vc));
   		
   		vc.jobType='';
   		System.assertEquals(defaultMaker.getRecordType('Permanent Vacancy','PeopleCloud1__Placement__c'),defaultMaker.getVacancyRecordType(vc));
   		
 
   		vc.jobType='other';
   		System.assertEquals(defaultMaker.getRecordType('Permanent Vacancy','PeopleCloud1__Placement__c'),defaultMaker.getVacancyRecordType(vc));
   		
   }
}