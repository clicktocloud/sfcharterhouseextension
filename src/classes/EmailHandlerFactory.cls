/**
* This factory creates corresponding email handler object which implements IEmailHandler interface 
* depending on the content of email.
* 
*/

public with sharing class EmailHandlerFactory {
	
	public static IEmailHandler build( Messaging.InboundEmail email ){
		//firstly try to get Vacancy maker
		IEmailHandler handler = getVacancyMaker( email );
		
		if(handler == null){
			//TODO try to create other type of email handler
			
			
			System.debug('Failed to create VacancyMaker according to inbound email');
		}
		
		return handler;
		
	}
	
	/**
	* create VacancyMaker object
	* return:
	*	IEmailHandler object
	*/
	public static IEmailHandler getVacancyMaker( Messaging.InboundEmail email ){
		
		IVacancyParser parser = VacancyParserFactory.getVacancyParser( email ); 
		if ( parser != null ){
			VacancyMaker maker = new VacancyMaker ( parser );
			maker.setEmail(email);
			
			return maker;
		}
		
		return null;
		
	}
	
    
    
}