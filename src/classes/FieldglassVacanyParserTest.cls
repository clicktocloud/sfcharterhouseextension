
@isTest
private class FieldglassVacanyParserTest {

   	private static Messaging.InboundEmail email;
    private static FieldglassVacancyParser parser; 
    private static VacancyContent content;
   
    private static void createFixture(){
        email = new Messaging.InboundEmail();
        email.subject = ' New Job Posting submitted [Job Posting ID: NSWEJP00001376, Job Posting Title: Personal Assistant]';
        email.fromAddress = 'somebody@somedomain.com';
        email.plainTextBody = 'Job Posting ID\n'+
				'NSWEJP00001376\n'+
				'Job Posting Title\n'+
				'Personal Assistant\n'+
				'Description\n'+
				'The role provide high level confidential executive, administrative and project support to the Executive Director.\n'+
				'Intermediate to advanced level of Word and Excel.\n'+
				'Critical to this role is exceptional interpersonal, organisational and time'+
				'management skills.\n'+
				'Job Posting Start Date\n'+
				'24/05/2016\n'+
				'Job Posting End Date\n'+
				'18/07/2016\n'+
				'Business Unit\n'+
				'Asset Management\n'+
				'Site\n'+
				'State Office-Sydney\n'+
				'Posting Information\n'+
				'Job Posting Owner\n'+
				'Michael Brown\n'+
				'This notification was sent by';
    }
    
    static{
        createFixture( );
        
        parser = new FieldglassVacancyParser(email);
        
        content = parser.parse();
    }
    
    
    
    static testMethod void testGetJobCode(){
        
        System.assertEquals('NSWEJP00001376',  parser.getJobCode());
        
    }
    static testMethod void testGetJobTitle(){
        
        System.assertEquals('Personal Assistant', parser.getJobTitle());
        
    }
    
    static testMethod void testGetJobType(){
        
        System.assertEquals('', parser.getJobType());
        
    }
    
    static testMethod void testGetStartDate(){
        
        System.assertEquals( Date.parse('24/05/2016'), parser.getStartDate());
        
    }
    
    static testMethod void testGetEndDate(){
        
        System.assertEquals(Date.parse('18/07/2016'), parser.getEndDate());
        
    }
    
    static testMethod void testGetPostedBy(){
        
        System.assertEquals('Michael Brown', parser.getPostedBy());
        
    }
    
     static testMethod void testGetReportTo(){
        
        System.assertEquals('', parser.getReportTo());
        
    }
    
     static testMethod void testGetPositions(){
        
        System.assertEquals(0, parser.getPositions());
        
    }
    
    static testMethod void testGetComment(){
        
        System.assertEquals('', parser.getComment());
        
    }
    
     static testMethod void testGetClient(){
        
        System.assertEquals('Fieldglass', parser.getClient());
        
    }
    
    
     static testMethod void testGetStatus(){
        
        System.assertEquals('Open', parser.getStatus());
        
    }
    
    
    
    static testMethod void testParse(){
        System.assert(content != null);
        System.assertEquals('NSWEJP00001376', content.jobCode);
        System.assertEquals('Personal Assistant', content.jobTitle);
        System.assertEquals(0, content.positions);
        System.assertEquals('Open', content.status);
        System.assertEquals('Fieldglass', content.client);
        System.assertEquals('Michael Brown', content.postedBy);
        System.assertEquals('', content.reportTo);
        
    }
}