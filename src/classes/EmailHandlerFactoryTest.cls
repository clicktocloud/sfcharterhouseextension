@isTest
private class EmailHandlerFactoryTest {
    
    static Map<String , Messaging.InboundEmail> emails  = new Map<String, Messaging.InboundEmail>();
   
    static{
        	Messaging.InboundEmail mail = new Messaging.InboundEmail();
       	//from Fieldglass
		mail.plainTextBody = 'A new Job Posting has been submitted';
	    emails.put('Fieldglass', mail);
	    //from domain
	    mail = new Messaging.InboundEmail();
	    mail.plainTextBody = 'The following Order has been re-opened';
	    emails.put('Comensura', mail);
	    
	    //unexpected email
	    mail = new Messaging.InboundEmail();
	    mail.plainTextBody = 'This is unexpected email\n';
	    emails.put('unknown', mail);
	    
	    /*
	    *by from address or subject
	    */
	    // Fieldglass email
	    mail = new Messaging.InboundEmail();
		mail.subject = 'New Job Posting submitted';
		mail.fromAddress = 'fieldglass@fieldglass.net' ;
		emails.put('Fieldglass-sub-from', mail);
		
		// Comensura email by subject
		mail = new Messaging.InboundEmail();
		mail.subject = 'position(s) required';
		mail.fromAddress = 'helpdesk@comensura.com.au';
		emails.put('Comensura-sub-from', mail);
	    
   }

	private static testMethod void testBuild() {
	    
	    IEmailHandler handler = EmailHandlerFactory.build(emails.get('Fieldglass'));
	    System.assert(handler instanceof VacancyMaker);
	    handler = EmailHandlerFactory.build(emails.get('Comensura'));
	    System.assert(handler instanceof VacancyMaker);

	    handler = EmailHandlerFactory.build(emails.get('Fieldglass-sub-from'));
	    System.assert(handler instanceof VacancyMaker);
	    handler = EmailHandlerFactory.build(emails.get('Comensura-sub-from'));
	    System.assert(handler instanceof VacancyMaker);
	  
	    
        handler = EmailHandlerFactory.build(emails.get('unknown'));
	    System.assert(handler == null);
	    
	    handler = EmailHandlerFactory.build(null);
	    System.assert(handler == null);

	}
	
	private static testMethod void testGetVacanyMaker() {
	    
	    IEmailHandler handler = EmailHandlerFactory.getVacancyMaker(emails.get('Fieldglass'));
	    System.assert(handler instanceof VacancyMaker);
	    handler = EmailHandlerFactory.getVacancyMaker(emails.get('Comensura'));
	    System.assert(handler instanceof VacancyMaker);
	    
	    handler = EmailHandlerFactory.getVacancyMaker(emails.get('Fieldglass-sub-from'));
	    System.assert(handler instanceof VacancyMaker);
	    handler = EmailHandlerFactory.getVacancyMaker(emails.get('Comensura-sub-from'));
	    System.assert(handler instanceof VacancyMaker);
	  
        handler = EmailHandlerFactory.getVacancyMaker(emails.get('unknown'));
	    System.assert(handler == null);
	    
	    handler = EmailHandlerFactory.getVacancyMaker(null);
	    System.assert(handler == null);

	}


}